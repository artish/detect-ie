DetectIE
========

Detect if the current Browser is IE.

## Useage:

```js
var isIE = detectIE();
```
